import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Animated,
} from 'react-native';

const Animacion1 = () => {

  // valor inicial 
  const [animacion] = useState(new Animated.Value(0));

  useEffect(() => {
    // Realiza una animacion en un determinado tiempo
    // recibe dos valores {valor al que va llegar, duracion} 
    Animated.timing(
      animacion, {
        toValue: 1, // valor al que va llegar
        duration: 500
      }
    ).start();
  }, [])
  

  return (
    <Animated.View
      style={{
        opacity: animacion
      }}
    >
      <Text style={styles.text}>Animacion1</Text>
    </Animated.View>
  )
}

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    textAlign: 'center'
  }
});

export default Animacion1