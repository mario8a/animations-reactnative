import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Animated,
} from 'react-native';

const Animacion3 = () => {

  // valor inicial 
  const [animacion] = useState(new Animated.Value(14));

  useEffect(() => {
    // Realiza una animacion en un determinado tiempo
    // recibe dos valores {valor al que va llegar, duracion} 
    Animated.timing(
      animacion, {
        toValue: 40, // valor al que va llegar
        duration: 500
      }
    ).start();
  }, [])
  

  return (
    <View>
      <Animated.Text 
        style={[styles.text, {fontSize: animacion}]}
      >Animacion1
      </Animated.Text>
    </View>
  )
}

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    textAlign: 'center'
  }
});

export default Animacion3