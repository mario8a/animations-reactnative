import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Animated,
} from 'react-native';

const Animacion4 = () => {

  // valor inicial 
  const [animacion] = useState(new Animated.Value(0));

  useEffect(() => {
    // Realiza una animacion en un determinado tiempo
    // recibe dos valores {valor al que va llegar, duracion} 
    Animated.timing(
      animacion, {
        toValue: 360, // valor al que va llegar
        duration: 500
      }
    ).start();
  }, []);

  const interpolacion = animacion.interpolate({
    inputRange: [0 , 360], // de que valor a que valor va a ir
    outputRange: ['0deg', '360deg']
  });

  const estiloAnimacion = {
    transform: [{rotate: interpolacion}]
  }
  

  return (
    <View>
      <Animated.View 
        style={[styles.caja, estiloAnimacion]}
      >
      </Animated.View>
    </View>
  )
}

const styles = StyleSheet.create({
  caja: {
    width: 100,
    height: 100,
    backgroundColor: 'cornflowerblue'
  }
});

export default Animacion4