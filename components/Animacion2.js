import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Animated,
} from 'react-native';

const Animacion2 = () => {

  // valor inicial 
  const [animacion] = useState(new Animated.Value(0));

  useEffect(() => {
    // Realiza una animacion en un determinado tiempo
    // recibe dos valores {valor al que va llegar, duracion} 
    Animated.timing(
      animacion, {
        toValue: 450, // valor al que va llegar
        duration: 1000
      }
    ).start();
  }, [])
  

  return (
    <Animated.View
      style={[
        styles.caja,
        {
          width: animacion,
          height: animacion
        }
      ]}
    >

    </Animated.View>
  )
}

const styles = StyleSheet.create({
  caja: {
    width: 100,
    height: 100,
    backgroundColor: 'cornflowerblue'
  }
});

export default Animacion2